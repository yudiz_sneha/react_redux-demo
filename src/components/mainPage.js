import React, { useEffect, useState } from "react";
import Order from "../components/orderPage";
import "../assets/styles/mainPage.css";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const Home = () => {
  const [data, setData] = useState([]);
  const [mainData, setMainData] = useState({});
  const [subData, setSubData] = useState([]);
  const [subMainData, setSubMainData] = useState({});
  const [showProducts, setShowProducts] = useState([]);
  const [showProductData, setShowProductData] = useState(false);
  const [productMain, setProductMain] = useState({});
  const [totalPrice, setTotalPrice] = useState(0);

  const products = useSelector(
    (state) => state.productData && state.productData.products
  );
  const categories = useSelector(
    (state) => state.data && state.data.categories
  );
  const basket = useSelector((state) => state.basket);
  const history = useHistory();
  useEffect(() => {
    setData(categories.filter((item) => item.parent === null));
    setSubData(categories.filter((item) => item.parent === 1));
  }, []);

  function handleData(id) {
    setSubData(categories?.filter((item) => item.parent === id));
    setMainData(categories?.find((item) => item.id === id));
  }
  function handleSubData(id) {
    setSubMainData(categories?.find((category) => category.id === id));
    setShowProducts(products.filter((product) => product.parentId === id));
  }
  useEffect(() => {
    setShowProducts(products.filter((item) => item.parentId === subData[0]?.id));
    setSubMainData(subData[0]);
  }, [subData]);

  useEffect(() => {
    setSubMainData(mainData[0]);
  }, [mainData]);

  function handleClick(item) {
    setShowProductData(true);
    setProductMain(item);
  }
  function handleBasket() {
    history.push("/basket");
  }
  useEffect(() => {
    const Price = basket.products.reduce((acu, curr) => {
      return acu + parseFloat(curr.totalPrice);
    }, 0);
    setTotalPrice(Price);
  }, [basket]);

  return (
  <>
    <div className="Home">
     <br/>
      <div className="main">
        <div className="title">
          <h1>Kings Arms Cardington</h1>
          <p>134 High Street, Kempston, Bedford, Bedfordshire, MK42 7BN</p>
          <div>
          </div>
        </div>
        <br/>
        <div className="firstitems">
          {data.map((item) => (
            <button
              key={item.id}
              className={mainData?.id === item.id ? "button active" : "button"}
              onClick={() => handleData(item.id)}
            >
              {item.name}
            </button>
          ))}
        </div>
        <div className="home">
          {subData?.map((item) => (
            <button
              key={item.id}
              onClick={() => handleSubData(item.id)}
              className={
                subMainData?.id === item.id ? 'underlined active'
                : 'underlined'
              }
            >
              {item.name}
            </button>
          ))}
        </div>
      </div>
        <br/>
      <div
        className={
          basket.products.length == 0 ? "homeproductlist" : "productlist"
        }
      >
        <div className="test">
          {showProducts?.map((item) => (
            <div
              className="product"
              key={item.id}
              onClick={() => handleClick(item)}
              setShowProductData={setShowProductData}
              setProductMain={setProductMain}
            >
              <div>
                <h1>{item.name}</h1>
                <p>{item.description}</p>
              </div>
              <div>£{item.price}</div>
            </div>
          ))}
        </div>
      </div>
      <>
      {basket.products.length == 0 ? (
        <></>
      ) : (
        <div className="bottom" onClick={handleBasket}>
          <div>VIEW BASKET</div>
          <div>
            £{totalPrice.toFixed(2)} / {basket.products.length} ITEM
          </div>
        </div>
      )}
      </>
    
      {showProductData && (
        <Order
          setShowProductData={setShowProductData}
          productMain={productMain}
        />
      )}
    </div>
    </>
  );
};

export default Home;
