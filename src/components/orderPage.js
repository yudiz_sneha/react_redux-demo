import React, { useEffect, useState } from "react";
import viewType from "../state/actions/index";
import Tick from "../assets/images/tick.png";
import "../assets/styles/order.css";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";

function Order({ setShowProductData, productMain }) {

  const [ qty, setQty ] = useState(1);
  const [ productOfVariant, setProductOfVariant ] = useState({});
  const [ productExtras, setProductExtras ] = useState([]);
  const [ productPrice, setProductPrice ] = useState(0);
  const [ category, setCategory ] = useState("");
  const categories = useSelector((state) => state.data?.categories);
  const [ product, setProduct ] = useState({
    name: productMain.name,
    totalPrice: productMain.price,
    qty,
    extras: [],
    variant: "",
    category,
  });

  const dispatch = useDispatch();

  useEffect(() => {
    if (productMain.variants !== undefined) {
      setProductOfVariant(productMain.variants[0]);
    }

    const category = categories.find((catgory) => catgory.id == productMain.parentId);
    const mainCatgory = categories.find((catgory) => catgory.id == category.parent);
    setCategory(mainCatgory.name);
  }, []);

  function handleOrder() {
    if (
      productMain.variants !== undefined &&
      productMain.extras !== undefined
    ) {
      dispatch(
        viewType.Basket.addDataToBasket({
          ...product,
          qty,
          category,
          extras: productExtras,
          variant: productOfVariant.name,
          totalPrice: qty * (productPrice + productOfVariant.price),
        })
      );

      setProduct({
        ...product,
        qty,
        category,
        extras: productExtras,
        variant: productOfVariant.name,
        totalPrice: qty * (productPrice + productOfVariant.price),
      });
    } 
    else if (
      productMain.variants === undefined &&
      productMain.extras === undefined
    ) {
      dispatch(
        viewType.Basket.addDataToBasket({
          ...product,
          qty,
          category,
          totalPrice: qty * productMain.price,
        })
      );
      setProduct({
        ...product,
        qty,
        category,
        totalPrice: qty * productMain.price,
      });
    } 
    else if (productMain.variants === undefined) {
      dispatch(
        viewType.Basket.addDataToBasket({
          ...product,
          qty,
          category,
          extras: productExtras,
          totalPrice: qty * (productPrice + productMain.price),
        })
      );
      setProduct({
        ...product,
        qty,
        category,
        extras: productExtras,
        totalPrice: qty * (productPrice + productMain.price),
      });
    } 
    else if (productMain.extras === undefined) {
      dispatch(
        viewType.Basket.addDataToBasket({
          ...product,
          qty,
          category,
          variant: productOfVariant.name,
          totalPrice: qty * productOfVariant.price,
        })
      );
      setProduct({
        ...product,
        qty,
        category,
        variant: productOfVariant.name,
        totalPrice: qty * productOfVariant.price,
      });
    }

    setShowProductData(false);
  }

  function handleCloseModalButton() {
    setShowProductData(false);
  }

  function handleOnChangeOnVariant(e) {
    const mainVariant = productMain.variants.find(
      (variant) => variant.name == e.target.value
    );
    setProductOfVariant(mainVariant);
  }

  const handleOnChangeOnExtras = (e) => {
    const { value, checked } = e.target;

    const onChangeProduct = productMain.extras.find(
      (extra) => extra.name == value
    );

    if (checked) {
      setProductExtras([...productExtras, onChangeProduct]);
      setProductPrice(productPrice + onChangeProduct.price);
    } 
    else {
      setProductExtras([
        ...productExtras.filter((extra) => extra.name !== value),
      ]);
      setProductPrice(productPrice - onChangeProduct.price);
    }
  };

  return (
    <div className="modal">
      <div className="content">
        <div className="contentname">
          <div style={{ fontSize: "20px" }}>{productMain.name}
            <p style={{ fontSize: "14px" }}>Lager</p>
          </div>
        
          <button className="btn active" onClick={handleCloseModalButton}>
            X
          </button>
        </div>

        <hr className="hproduct" />

        {productMain.variants && (
          <>
            <div className="variant">
              <div style={{ fontSize: "20px" }}>Size</div>

              {productMain.variants.map((variant, index) => (
                <div key={index} className="box">
                  <input
                    type="radio"
                    id={variant.name}
                    name="variant"
                    value={variant.name}
                    checked={productOfVariant == variant}
                    onChange={handleOnChangeOnVariant}
                    style={{ borderRadius: "8px" }}
                  />
                  <label className="variant-name" htmlFor={variant.name}>
                    {variant.name}
                  </label>
                  <label className="variant-price" htmlFor={variant.name}>
                    £{variant.price}
                  </label>
                </div>
              ))}
            </div>

            <hr className="hproduct" />
          </>
        )}

        {productMain.extras && (
          <>
            <div className="options">
              <div>Select Options</div>

              {productMain.extras.map((extra, index) => (
                <div key={index} className="option__box">
                  <label htmlFor={extra.name}>
                    {extra.name} (+ £{extra.price})
                  </label>
                  <div className="checkbox">
                    <input
                      className="input1"
                      type="checkbox"
                      id={extra.name}
                      name={extra.name}
                      value={extra.name}
                      onChange={handleOnChangeOnExtras}
                    />
                    <div className="checkbox__img">
                      <img className="img_1" src={Tick} alt="Tick" />
                    </div>
                  </div>
                </div>
              ))}
            </div>

            <hr className="hproduct" />
          </>
        )}

        <div className="content__qty">
          <button
            className="btn active"
            onClick={() => qty > 1 && setQty(qty - 1)}
          >
            -
          </button>
          <h1 className="qty">{qty}</h1>
          <button className="btn active" onClick={() => setQty(qty + 1)}>
            +
          </button>
        </div>

        <div className="content__btn">
          <button className="orderBtn" onClick={handleOrder}>
            Add to Order
          </button>
        </div>
        <br/>
      </div>
    </div>
  );
}

Order.propTypes = {
  setShowProductData: PropTypes.func,
  productMain: PropTypes.object,
};

export default Order;