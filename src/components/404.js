import React from 'react'
import { Link } from 'react-router-dom'

function PageNotFound() {
	return (
		<div className='page_404'>
			<h1>404</h1>
			<h2>Page not found</h2>
			<Link to='/'>
				<input type="button" />GO HOME
			</Link>
		</div>
	)
}

export default PageNotFound
