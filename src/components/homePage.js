import React from "react";
import Basket from "../components/basketPage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "../assets/styles/homePage.css";
import Home from "../components/mainPage";

const Homepage = () => {
  return (
    <div className="page">
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/basket" component={Basket} />
        </Switch>
      </Router>
    </div>
  );
};

export default Homepage;
