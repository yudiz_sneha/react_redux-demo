import React from "react";
import Homepage from "./components/homePage";


function App() {
  return (
    <div className="App">
      <Homepage />
    </div>
  );
}

export default App;
