import { applyMiddleware, createStore } from "redux";
import Reducers from "../state/reducers";
import thunk from "redux-thunk";

const store = createStore(Reducers, {}, applyMiddleware(thunk));
export default store;
