import { combineReducers } from "redux";
import showBasket from "./showBasket";
import showTheData from "./showItem";
import showProducts from "./showProduct";

const Reducer = combineReducers({
  data: showTheData,
  productData: showProducts,
  basket: showBasket,
});
export default Reducer;
