import category from "./categoryAction";
import Products from "./productAction";
import Basket from "./basketAction";

const viewType = {
  category,
  Products,
  Basket,

};
export default viewType;
